FROM  python:3.7

WORKDIR /prod
COPY . .
CMD ["python3", "run_prod.py"]
