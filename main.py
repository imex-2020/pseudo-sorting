import config
import hashlib
import os

def main(stdin, stdout):
    stdout.write('Oi, você já sabe a ordem dos maratoneiros no lab?\n')
    stdout.write('Manda um arquivo com as informações aí!\n')
    stdout.write(f'Só fazer \'nc imesec.ime.usp.br {config.PORT} < resposta.txt\'\n')
    stdout.flush()

    names = stdin.readline().strip('\n').split(',')
    final_names = []
    for name in names:
        if len(name.strip()) != 0:
            final_names.append(name.strip())
    resp = ','.join(final_names)

    md5hash = hashlib.md5(resp.encode('utf-8')).hexdigest()
    if md5hash == config.HASH:
        stdout.write(f'Parece tudo correto, toma isso aqui: {os.environ["FLAG"]}\n')
    else:
        stdout.write('Algo me diz que você errou algum detalhe\n')
        stdout.write('Tenta um pouco mais e pode voltar aqui mais tarde\n')
    stdout.flush()
