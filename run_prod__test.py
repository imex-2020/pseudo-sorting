import run_prod

class TestCases:
    class StringSocket:
        def __init__(self, string: str):
            self.string = string
            self.closed = False
            self.index = 0

        def recv(self, buffer_size: int):
            if self.closed:
                raise Exception('Tried to read from closed StringSocket')
            to_send = self.string[self.index:self.index+buffer_size]
            self.index += len(to_send)
            return bytes(to_send, 'utf8')

        def close(self):
            self.closed = True

    def test__stdin_readline(self):

        testcases = [
            {
                'name': 'Buffer size before \\n',
                'chunk_size': 3,
                'input_io': 'abcde\ndefgh',
            },
            {
                'name': 'Buffer size in \\n',
                'chunk_size': 6,
                'input_io': 'abcde\ndefgh'
            },
            {
                'name': 'Buffer size greater than \\n',
                'chunk_size': 8,
                'input_io': 'abcde\ndefgh'
            },
        ]

        for testcase in testcases:
            name = testcase.get('name', '')
            input_io = testcase.get('input_io', '')
            chunk_size = testcase.get('chunk_size', 0)
            socket = self.StringSocket(input_io)

            stdin = run_prod.SocketWrapper.Stdin(socket, chunk_size)
            expected = input_io.split('\n')
            for s in expected:
                got = stdin.readline()
                assert s == got, Exception(f'Error in testcase {name}: expected "{s}" | got "{got}"')

if __name__ != '__main__':
    raise Exception('Cannot import test module')
testcases = TestCases()
for test_name in [t for t in dir(testcases) if t.startswith('test__')]:
    test = testcases.__class__.__dict__[test_name]
    try:
        test(testcases)
        print(f'[PASS] {test_name}')
    except Exception as e:
        print(f'[FAIL] {test_name}', end=': ')
        print(e)